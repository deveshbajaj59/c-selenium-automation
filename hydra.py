
from selenium.webdriver import Chrome
from selenium.webdriver.common.keys import Keys


def automate(fname, lname, dateOfbirth):
    driver = Chrome("./Hydra/chromedriver")
    driver.get("https://app.cloudqa.io/home/AutomationPracticeForm#")
    name = driver.find_element_by_xpath(
        '/html/body/div[1]/form/div[1]/div[2]/input')
    name.clear()
    name.send_keys(fname)
    Lname = driver.find_element_by_xpath(
        '/html/body/div[1]/form/div[1]/div[3]/input')
    Lname.clear()
    Lname.send_keys(lname)
    dob = driver.find_element_by_xpath(
        '/html/body/div[1]/form/div[1]/div[5]/input')
    dob.clear()
    dob.send_keys(dateOfbirth)
    check = driver.find_element_by_xpath(
        '/html/body/div[1]/form/div[3]/div/input')
    check.click()
    submit = driver.find_element_by_xpath(
        '/html/body/div[1]/form/div[4]/button[1]')
    submit.click()
    driver.close()
    driver.quit()


if __name__ == "__main__":
    fname = [
        "Aaren",
        "Aarika",
        "Abagael",
        "Abagail",
        "Abbe",
        "Abbey",
        "Abbi",
        "Abbie",
        "Abby",
        "Abbye",
        "Abigael"]
    lname = ["Adorne",
             "Adrea",
             "Adria",
             "Adriaens",
             "Adrian",
             "Adriana",
             "Adriane",
             "Adrianna",
             "Adrianne",
             "Adriena",
             "Adriena"]
    dob = [
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
        "1993-11-23",
    ]
    for i in range(len(dob)):
        automate(fname[i], lname[i], dob[i])
