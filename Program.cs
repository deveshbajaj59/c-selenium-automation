﻿using System;
using System.IO;
using System.Reflection;
// // using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;


namespace Hydra
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World! devesh");
            var browserDriverPath = "/home/bajajdk/Documents/personal/Hydra/";//Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Console.WriteLine(browserDriverPath);
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--start-maximized");
            
            using (var chromeDriver = new ChromeDriver(browserDriverPath, options))
            {
                //Assign google.com to variable named url
                var url = "https://app.cloudqa.io/home/AutomationPracticeForm#";
                //Go to Google.com
                chromeDriver.Navigate().GoToUrl(url);

                //Create new wait timer and set it to 1 minute
                var wait = new WebDriverWait(chromeDriver, new TimeSpan(0, 0, 1, 0));
                var name = chromeDriver.FindElement(By.XPath("/html/body/div[1]/form/div[1]/div[2]/input"));
                //clear search box just in case anything is already typed in
                name.Clear();
                //Type "Selenium HQ" into the search box
                name.SendKeys("Selenium HQ");
                
                var lname = chromeDriver.FindElement(By.XPath("/html/body/div[1]/form/div[1]/div[3]/input"));
                //clear search box just in case anything is already typed in
                lname.Clear();
                //Type "Selenium HQ" into the search box
                lname.SendKeys("Selenium HQ");

                var dob = chromeDriver.FindElement(By.XPath("/html/body/div[1]/form/div[1]/div[5]/input"));
                //clear search box just in case anything is already typed in
                dob.Clear();
                //Type "Selenium HQ" into the search box
                dob.SendKeys("1997-01-12");

                var agree = chromeDriver.FindElement(By.XPath("/html/body/div[1]/form/div[3]/div/input"));
                //clear search box just in case anything is already typed in
                //Type "Selenium HQ" into the search box
                agree.Click();
                


                //Wait until Google Search button is visible but don't wait more than 1 minute.
                // wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Name("btnK")));
                //Find "Google Search" button and assign to variable name "searchButton" //*[@id="fname"]
                var searchButton = chromeDriver.FindElement(By.XPath("/html/body/div[1]/form/div[4]/button[1]"));

                //Click search button
                Console.WriteLine(searchButton);
                searchButton.Click();
                    
                //wait until search results stats appear which confirms that the search finished
                // wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("resultStats")));

                // //Find result stats and assign to variable name resultStats
                // var resultStats = chromeDriver.FindElement(By.Id("resultStats"));

                // wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Name("btnK")));
                chromeDriver.Close();
            }


        } 
    }
}
